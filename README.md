
react-miraz-shad-1
==================

Sample react project (Cefalo School Assignment 1)

How To Run
----------

Open terminal in the project directory and try the following command:

`npm install`

Then just do the following:

`npm start`

Contributor
------------

Ashraful Karim Miraz ([@miraz235](https://bitbucket.org/miraz235))

Shad Mazumder ([@shadmazumder](https://bitbucket.org/shadmazumder))

