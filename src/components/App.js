import React, {Component} from 'react'
import AppHeader from './AppHeader'
import ProductApp from './ProductApp'
import './App.css'

class App extends Component {
    render() {
        return (
            <div className="wrapper">
                <AppHeader />
                <ProductApp />
            </div>
        );
    }
}
export default App
