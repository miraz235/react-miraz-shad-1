import React, {Component} from 'react'

export default class Header extends Component {
  render() {
    return (
      <header className="container">
        <h2>Product App</h2>
      </header>
    )
  }
}