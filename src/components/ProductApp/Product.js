import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Shiitake from 'shiitake';

export default class Product extends PureComponent{
  static propTypes = {
    product: PropTypes.object.isRequired,
    onFav: PropTypes.func.isRequired,
    onFilter: PropTypes.func
  }
  handleFav = () => {
    console.log('click 1')
    this.props.onFav(this.props.product.id)
  }
  render() {
    const {id, isFavourite, tag, image, title, color} = this.props.product
    return (
      <div className="column is1of4">
        <div className="card">
          <div className="card-image" style={{backgroundImage: `url(${image})`}}>
            <a className={isFavourite ? 'card-image-fav active text-btn' : 'card-image-fav text-btn'} onClick={this.props.onFav(id)}>
              <svg className="icon-heart" viewBox="0 0 32 32" role="img" strokeLinecap="round" strokeLinejoin="round"><path d="M23.993 2.75c-.296 0-.597.017-.898.051-1.14.131-2.288.513-3.408 1.136-1.23.682-2.41 1.621-3.688 2.936-1.28-1.316-2.458-2.254-3.687-2.937-1.12-.622-2.268-1.004-3.41-1.135a7.955 7.955 0 0 0-.896-.051C6.123 2.75.75 4.289.75 11.128c0 7.862 12.238 16.334 14.693 17.952a1.004 1.004 0 0 0 1.113 0c2.454-1.618 14.693-10.09 14.693-17.952 0-6.84-5.374-8.378-7.256-8.378"></path></svg>
            </a>
          </div>
          <a className="card-tag text-btn" style={{color: color}} onClick={this.props.onFilter(tag)}>{tag}</a>
          <Shiitake lines={2} throttleRate={200} className="card-title" tagName="h3">
            {title}
          </Shiitake>
        </div>
      </div>
    )
  }
}