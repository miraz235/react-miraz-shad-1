import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import ProductCategory from './ProductCategory'

export default class Categories extends PureComponent{
  static propTypes = {
    categories: PropTypes.array.isRequired,
    onFav: PropTypes.func.isRequired,
    onFilter: PropTypes.func
  }
  render() {
    const categories = this.props.categories
    return categories.map((category, index) => category.data.length ? (
        <ProductCategory category={category} onFav={this.props.onFav} onFilter={this.props.onFilter} key={index} />
    ) : false)
  }
}