import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Products from './Products'

export default class Category extends PureComponent{
  static propTypes = {
    category: PropTypes.object.isRequired,
    onFav: PropTypes.func.isRequired,
    onFilter: PropTypes.func
  }
  render() {
    const {name, data} = this.props.category
    return (
      <section className="section">
        <h2>{name}</h2>
        <Products products={data} onFav={this.props.onFav} onFilter={this.props.onFilter} />
      </section>
    )
  }
}