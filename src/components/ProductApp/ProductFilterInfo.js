import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'

export default class Product extends PureComponent{
  static propTypes = {
    filter: PropTypes.object.isRequired,
    onFilter: PropTypes.func
  }
  render() {
    let filterDom = []
    if(this.props.filter && this.props.filter.name) {
      const {name, value} = this.props.filter
      filterDom.push(
        <div className="filter-container">
          <h4>Filter By: <key>{name}</key> <code>{value}</code> <a onClick={this.props.onFilter}><big>&times;</big></a></h4>
        </div>
      )
    }
    return filterDom
  }
}