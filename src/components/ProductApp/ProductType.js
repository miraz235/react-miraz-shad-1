import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'

export default class ProductType extends PureComponent{
  static propTypes = {
    type: PropTypes.object.isRequired,
    onFilter: PropTypes.func.isRequired
  }
  render() {
    const {image, type} = this.props.type
    return (
      <div className="column">
        <div className="card card--inline">
          <button className="text-btn" onClick={this.props.onFilter(type)}>
            <div className="card-avatar">
              <div className="card-image" style={{backgroundImage: `url(${image})`}}></div>
            </div>
            <div className="card-label">{type}</div>
          </button>
        </div>
      </div>
    )
  }
}