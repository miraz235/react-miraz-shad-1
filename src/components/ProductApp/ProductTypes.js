import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import ProductType from './ProductType'

export default class ProductTypes extends PureComponent{
  static propTypes = {
    types: PropTypes.array.isRequired,
    onFilter: PropTypes.func.isRequired
  }
  render() {
    const types = this.props.types
    return (
      <div className="columns no-wrap">
      { types.map((product) => (
        <ProductType key={product.id} type={product} onFilter={this.props.onFilter} />
        ))
      }
      </div>
    )
  }
}