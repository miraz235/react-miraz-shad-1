import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import Product from './Product'

export default class Products extends PureComponent{
  static propTypes = {
    products: PropTypes.array.isRequired,
    onFav: PropTypes.func.isRequired,
    onFilter: PropTypes.func
  }
  render() {
    const products = this.props.products
    return (
      <div className="columns">
      { 
        products.map((product) => (
          <Product key={product.id} product={product} onFav={this.props.onFav} onFilter={this.props.onFilter} />
        ))
      }
      </div>
    )
  }
}