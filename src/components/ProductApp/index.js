import React, {Component} from 'react'
import ProductFilterInfo from './ProductFilterInfo'
import ProductTypes from './ProductTypes'
import ProductCategories from './ProductCategories'

import './ProductApp.css'
const productsData = require('./products.json')

export default class ProductApp extends Component{
  products = []
  productCategories = []
  productTypes = []
  productFavCategory = 'Favourites'
  
  state = {
    filterBy: {},
    productsInCategories: []
  }

  componentDidMount = () => {
    const pData = this.getProducts(productsData)
    this.productCategories = this.getCategories(pData)
    this.productTypes = this.getTypes(pData)
    this.products = pData
    this.handleResetFilterProduct()
  }

  getTypes = (products) => {
    let flagTypes = [];
    return products.filter((product) => {
      if(flagTypes.indexOf(product.type)===-1) {
        flagTypes.push(product.type)
        return product;
      }
      return false
    })
  }

  getProducts = (products) => {
    const colors = ['#E02A42', '#DD3C14', '#9A613B', '#D88C09', '#271F14', '#21B2A3', '#335D6D', '#2A5B2D', '#93704D', '#4393D0', '#B8D076', '#706627', '#E4AF33', '#073FA1']
    return products.map(product => {
      return {
        ...product,
        color: colors[Math.floor(Math.random() * colors.length)],
        categories: product.isFavourite ? [...product.categories, this.productFavCategory] : product.categories
      }
    })
  }

  getCategories = (products) => {
    let productCategories = products.reduce((prev, curr) => {
      const newer = curr.categories.filter((category) => {
        return !prev.find(pcat => pcat === category)
      })
      return [...prev, ...newer]
    },[])
    productCategories.splice(productCategories.indexOf(this.productFavCategory), 1)
    productCategories.push(this.productFavCategory)
    return productCategories
  }

  makeCategoryProducts = (products, filterBy) => {
    return this.productCategories.map(category => {
      return { 
        name: category,
        data: products.filter((product) => 
          product.categories.indexOf(category) > -1 && 
          (filterBy && filterBy.name ? product[filterBy.name] === filterBy.value : true))
      }
    })
  }

  setFilteredProduct = (filterBy) => {
    this.setState({
      filterBy,
      productsInCategories: this.makeCategoryProducts(this.products, filterBy)
    })
  }
  
  handleFavoriteProduct = (productId) => () => {
    const newProductSet = this.products.map((product) => {
      if(product.id === productId) {
        return {
          ...product,
          isFavourite: !product.isFavourite,
          categories: !product.isFavourite ? [...product.categories, this.productFavCategory] : (() => {
            product.categories.splice(product.categories.indexOf(this.productFavCategory), 1)
            return product.categories
          })()
        }
      }
      return product
    })
    this.products = newProductSet
    this.setFilteredProduct(this.state.filterBy)
  }
  
  handleTypeFilterProduct = (typeName) => () => {
    const filterBy = { name: 'type', value: typeName }
    this.setFilteredProduct(filterBy)
  }

  handleTagFilterProduct = (tagName) => () => {
    const filterBy = { name: 'tag', value: tagName }
    this.setFilteredProduct(filterBy)
  }

  handleResetFilterProduct = () => {
    const filterBy = { name: '', value: '' }
    this.setFilteredProduct(filterBy)
  }

  render() {
    const {filterBy, productsInCategories} = this.state
    return (
      <div className="page-container">
        <ProductFilterInfo filter={filterBy} onFilter={this.handleResetFilterProduct} />
        <div className="container">
          <ProductTypes types={this.productTypes} onFilter={this.handleTypeFilterProduct} />
          <ProductCategories categories={productsInCategories} onFav={this.handleFavoriteProduct} onFilter={this.handleTagFilterProduct} />
        </div>
      </div>
    )
  }
}